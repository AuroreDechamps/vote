package com.example.decha.vote_sur_tablette.Metier;

/**
 * Created by Admin on 10/05/2016.
 */
public class ItemMenuPrincipale {

    private int IndiceMenu;
    private String Titre;
    private int IdPhotofromDrawable;

    public ItemMenuPrincipale(int IndiceMenu,String Titre, int IDPhotofromDrawable){

        this.IndiceMenu=IndiceMenu;
        this.Titre=Titre;
        this.IdPhotofromDrawable=IDPhotofromDrawable;

    }



    public int getIndiceMenu() {
        return IndiceMenu;
    }

    public void setIndiceMenu(int indiceMenu) {
        IndiceMenu = indiceMenu;
    }

    public String getTitre() {
        return Titre;
    }

    public void setTitre(String titre) {
        Titre = titre;
    }

    public int getIdPhotofromDrawable() {
        return IdPhotofromDrawable;
    }

    public void setIdPhotofromDrawable(int idPhotofromDrawable) {
        IdPhotofromDrawable = idPhotofromDrawable;
    }
}
