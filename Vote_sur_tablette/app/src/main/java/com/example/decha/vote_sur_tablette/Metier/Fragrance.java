package com.example.decha.vote_sur_tablette.Metier;

/**
 * Created by macbookpro on 22/05/2016.
 */
public class Fragrance {


    private String title;

    private String imagePath;

    public Fragrance(String imagePath, String title) {
        this.imagePath = imagePath;
        this.title = title;
    }

    public String getDescription() {
        return title;
    }

    public String getImagePath() {
        return imagePath;
    }

}