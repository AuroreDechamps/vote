package com.example.decha.vote_sur_tablette;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.*;
import android.widget.Toast;

/**
 * Created by decha on 18/02/2016.
 */
public class MesFlaconsActivity extends AppCompatActivity {
    public final static String KEY_EXTRA_CONTACT_ID = "KEY_EXTRA_CONTACT_ID";

    private ListView listView;
    VoteDBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mes_flacons);

        Button button = (Button) findViewById(R.id.addNew);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MesFlaconsActivity.this, CreateOrEditActivity.class);
                intent.putExtra(KEY_EXTRA_CONTACT_ID, 0);
                startActivity(intent);
            }
        });

        dbHelper = new VoteDBHelper(this);

        final Cursor cursor = dbHelper.getAllFlacons();
        String [] columns = new String[] {
                VoteDBHelper.FLACONS_COLUMN_ID,
                VoteDBHelper.FLACONS_COLUMN_NAME,
                //VoteDBHelper.FLACONS_COLUMN_PICTURE
        };
        int [] widgets = new int[] {
                R.id.flaconID,
                R.id.flaconName,
               // R.id.image
        };



        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(this, R.layout.flacon_info,
                cursor, columns, widgets,cursor.getCount());
        listView = (ListView)findViewById(R.id.listView1);
        listView.setAdapter(cursorAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> listView, View view,
                                    int position, long id) {
                Toast.makeText(MesFlaconsActivity.this,"hajar",Toast.LENGTH_SHORT).show();
                Cursor itemCursor = (Cursor) MesFlaconsActivity.this.listView.getItemAtPosition(position);
                int flaconID = itemCursor.getInt(itemCursor.getColumnIndex(VoteDBHelper.FLACONS_COLUMN_ID));
                Intent intent = new Intent(getApplicationContext(), CreateOrEditActivity.class);
                intent.putExtra(KEY_EXTRA_CONTACT_ID, flaconID);
                startActivity(intent);
                Toast.makeText(MesFlaconsActivity.this,"la position est de"+position,Toast.LENGTH_SHORT).show();
            }
        });

    }

}
