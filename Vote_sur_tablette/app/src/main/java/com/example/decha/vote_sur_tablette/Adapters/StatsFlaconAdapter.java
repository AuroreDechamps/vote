package com.example.decha.vote_sur_tablette.Adapters;

import android.content.ClipData;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.decha.vote_sur_tablette.DAO.DecodeJson;
import com.example.decha.vote_sur_tablette.DAO.VtNameUtils;
import com.example.decha.vote_sur_tablette.Metier.Flacons;
import com.example.decha.vote_sur_tablette.Metier.ItemStats;
import com.example.decha.vote_sur_tablette.Metier.Questionnaire;
import com.example.decha.vote_sur_tablette.R;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hajar on 17/05/2016.
 */
public class StatsFlaconAdapter extends BaseAdapter
{
    Context MyContext;


    ArrayList<ItemStats> ListsStats=new ArrayList<ItemStats>();
    ArrayList<Flacons> ListFlacons=new ArrayList<Flacons>();
    public StatsFlaconAdapter(Context _MyContext,int idQuestionnaire)
    {
        MyContext = _MyContext;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        //ListeVotes= DecodeJson.getAllVotes();
        ListsStats=DecodeJson.getAllStatistiquesByIdQuestionnaire(idQuestionnaire);
        ListFlacons=DecodeJson.getAllFlaconsByIdQuestionnaire(idQuestionnaire);
    }

    @Override
    public int getCount()
    {
        return ListFlacons.size();
    }

    public Flacons getFlaconById(int idFlacon){
        for (   int i=0;i<ListFlacons.size();i++){
            Flacons f=ListFlacons.get(i);
            if (    f.getID()==idFlacon){
                return f;
            }

        }
        return null;
    }
    public ItemStats getItemStatsById(int idFlacon){
        for (   int i=0;i<ListsStats.size();i++){
            ItemStats f=ListsStats.get(i);
            if (    f.getIdFlacon()==idFlacon){
                return f;
            }

        }
        return null;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View MyView = convertView;
        if ( convertView == null )
        {

            Flacons  itemflacon=ListFlacons.get(position);

            LayoutInflater li = LayoutInflater.from(MyContext);
            MyView = li.inflate(R.layout.item_stats_flacon, null);//ya ilahi
            // Add The Text!!!
            TextView tv = (TextView)MyView.findViewById(R.id.text_flacon_item_stats);
            String titre=itemflacon.getName();
            tv.setText(titre);
            ImageView imgView = (ImageView)MyView.findViewById(R.id.image_flacon_item_stats);
            URL url = null;
            Bitmap bmp = null;
            try {
                String path = "http://"+ VtNameUtils.DATABASE_HOST+"/"+VtNameUtils.SERVER_DIRECTORY_NAME +"/"+VtNameUtils.SERVER_PHOTOS_DIRECTORY_NAME+"/";
                url = new URL(path+itemflacon.getPicture());
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }catch (IOException e) {
                e.printStackTrace();
            }
            imgView.setImageBitmap(bmp);
            //lb_nb_vote
            // nb vote

            ItemStats itmStat=getItemStatsById(itemflacon.getID());
            TextView tvNbVote = (TextView)MyView.findViewById(R.id.lb_nb_vote);
            if ( itmStat!=null ){
                tvNbVote.setText(itmStat.getNbVote()+" votes");
            }else{
                tvNbVote.setText("0 vote");
            }

            // imgView.setImageResource(itemVote.getIdPhotofromDrawable());
        }
        return MyView;
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }
}