package com.example.decha.vote_sur_tablette;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.decha.vote_sur_tablette.Adapters.AdmListeQuestionnaireItemAdapter;
import com.example.decha.vote_sur_tablette.DAO.DecodeJson;

public class AdminListeQuestionnaireActivity extends AppCompatActivity {
    ListView ListViewVoteAdmin;
    public long idSelected=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_liste_questionnaires);

        ListViewVoteAdmin=(ListView)findViewById(R.id.list_view_admin_votes);
        ListViewVoteAdmin.setAdapter(new AdmListeQuestionnaireItemAdapter(this));


        ListViewVoteAdmin.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                idSelected=ListViewVoteAdmin.getAdapter().getItemId(position);

                Toast.makeText(AdminListeQuestionnaireActivity.this," ya rab "+ListViewVoteAdmin.getAdapter().getItemId(position),Toast.LENGTH_SHORT).show();
              AlertDialog.Builder builder = new AlertDialog.Builder(AdminListeQuestionnaireActivity.this);
                builder.setTitle("Options");
                builder.setItems(new CharSequence[]
                                {"Liste", "Modifier", "Supprimer","Statistiques" , "Annuler"},
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // The 'which' argument contains the index position
                                // of the selected item
                                switch (which) {
                                    case 0:

                                        break;
                                    case 1:

                                        break;
                                    case 2:
                                        confirmSupprssion();
                                        break;
                                    case 3:
                                        Intent ntstat= new Intent(AdminListeQuestionnaireActivity.this, SimpleFlaconStatsActivity.class);
                                        AdminListeQuestionnaireActivity.this.startActivityForResult(ntstat, 19921);
                                        break;
                                    case 4:

                                        break;
                                }
                            }
                        });
              //  builder.create().show();

               /* AlertDialog.Builder ad = new AlertDialog.Builder(
                        AdminListeQuestionnaireActivity.this);
                ad.setIcon(R.drawable.add);
                ad.setTitle("Voulez-vous vraiment le supprimer ??!!");
                ad.setPositiveButton("oui", new android.content.DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {

                    }
                });;
                ad.setNegativeButton("Non", new android.content.DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {

                    }
                });
                ad.show();*/

            }

            private boolean confirmSupprssion(){
                boolean ok=false;
                AlertDialog.Builder ad = new AlertDialog.Builder(
                        AdminListeQuestionnaireActivity.this);
                ad.setIcon(R.drawable.quitter);
                ad.setTitle("Voulez-vous vraiment le supprimer ??!!");
                ad.setPositiveButton("oui", new android.content.DialogInterface.OnClickListener() {

               public void onClick(DialogInterface dialog,
                                        int which) {



                    }
                });

                ad.setNegativeButton("Non", new android.content.DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {

                    }
                });
                ad.show();

                return ok;
            }

        });

    }
}
