package com.example.decha.vote_sur_tablette.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.decha.vote_sur_tablette.DAO.DecodeJson;
import com.example.decha.vote_sur_tablette.DAO.VtNameUtils;
import com.example.decha.vote_sur_tablette.Metier.Questionnaire;
import com.example.decha.vote_sur_tablette.R;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Admin on 10/05/2016.
 */
public class AdmListeQuestionnaireItemAdapter extends BaseAdapter
{
    Context MyContext;

    ArrayList<Questionnaire> ListeQuestionnaire=new ArrayList<Questionnaire>();

    public AdmListeQuestionnaireItemAdapter(Context _MyContext)
    {
        MyContext = _MyContext;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        ListeQuestionnaire=DecodeJson.getAllVotes();
    }

    @Override
    public int getCount()
    {
        return ListeQuestionnaire.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View MyView = convertView;

        if ( convertView == null )
        {
            Questionnaire itemQuestionnaire=ListeQuestionnaire.get(position);
            LayoutInflater li = LayoutInflater.from(MyContext);
            MyView = li.inflate(R.layout.questionnaire_item_admin, null);
            // Add The Text!!!
            TextView tv = (TextView)MyView.findViewById(R.id.text_vote_item_admin);
            String titre=itemQuestionnaire.getTitreQuestionnaire();
            tv.setText(titre);
            ImageView imgView = (ImageView)MyView.findViewById(R.id.image_vote_item_admin);
            URL url = null;
            Bitmap bmp = null;
            try {
                String path = "http://"+ VtNameUtils.DATABASE_HOST+"/"+VtNameUtils.SERVER_DIRECTORY_NAME +"/"+VtNameUtils.SERVER_PHOTOS_DIRECTORY_NAME+"/";
                url = new URL(path+itemQuestionnaire.getFotoQuestionnaire());
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }catch (IOException e) {
                e.printStackTrace();
            }
            imgView.setImageBitmap(bmp);
           // imgView.setImageResource(itemVote.getIdPhotofromDrawable());
        }
        return MyView;
    }

    @Override
    public Object getItem(int position) {
        return ListeQuestionnaire.get(position);
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }
}