package com.example.decha.vote_sur_tablette.Metier;


public class Flacons {
    
    private int _ID;
    private String Name;
    private String Picture;

    public Flacons() {
    }

    public Flacons(int _ID, String Name, String Picture) {
        this._ID = _ID;
        this.Name = Name;
        this.Picture = Picture;
    }

    public int getID() {
        return _ID;
    }

    public void setID(int _ID) {
        this._ID = _ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getPicture() {
        return Picture;
    }

    public void setPicture(String Picture) {
        this.Picture = Picture;
    }
    
    
    
}
