package com.example.decha.vote_sur_tablette.Metier;

public class User {

	int id;
	String nom,email;
        
        
        public User() {
        }
	
        public User(int id, String nom, String email) {
            this.id = id;
            this.nom = nom;
            this.email = email;
        }

	@Override
	public String toString() {
		
		return "user with id = "+id;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
}
