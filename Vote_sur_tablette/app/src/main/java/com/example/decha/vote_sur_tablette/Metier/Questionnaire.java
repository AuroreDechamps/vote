package com.example.decha.vote_sur_tablette.Metier;

/**
 * Created by Admin on 10/05/2016.
 */
public class Questionnaire {

    private int IDQuestionnaire;
    private String TitreQuestionnaire;
    private String FotoQuestionnaire;

    public int getIDQuestionnaire() {
        return IDQuestionnaire;
    }

    public void setIDQuestionnaire(int IDQuestionnaire) {
        this.IDQuestionnaire = IDQuestionnaire;
    }

    public String getTitreQuestionnaire() {
        return TitreQuestionnaire;
    }

    public void setTitreQuestionnaire(String titreQuestionnaire) {
        TitreQuestionnaire = titreQuestionnaire;
    }

    public String getFotoQuestionnaire() {
        return FotoQuestionnaire;
    }

    public void setFotoQuestionnaire(String fotoQuestionnaire) {
        FotoQuestionnaire = fotoQuestionnaire;
    }
}
