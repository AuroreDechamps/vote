package com.example.decha.vote_sur_tablette.Adapters;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.decha.vote_sur_tablette.DAO.DbHelper;
import com.example.decha.vote_sur_tablette.DAO.DecodeJson;
import com.example.decha.vote_sur_tablette.DAO.VtNameUtils;
import com.example.decha.vote_sur_tablette.Metier.Flacons;
import com.example.decha.vote_sur_tablette.R;

import android.util.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hajar on 10/04/2016.
 */
public class VoteImageItemAdapter extends BaseAdapter
{
    Context MyContext;
    ArrayList<Flacons> _ListFlacons;
    public VoteImageItemAdapter(Context _MyContext,int idQuestionnaire)
    {
        MyContext = _MyContext;
        _ListFlacons= DecodeJson.getAllFlaconsByIdQuestionnaire(idQuestionnaire);
    }

    @Override
    public int getCount()
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
	                        /* Set the number of element we want on the grid */
        return _ListFlacons.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View MyView = convertView;

        if ( convertView == null )
        {
	          /*we define the view that will display on the grid*/
            //Inflate the layout

            LayoutInflater li = LayoutInflater.from(MyContext);//MyContext.getLayoutInflater();
            MyView = li.inflate(R.layout.flacon_item, null);

            // Add The Text!!!
            TextView tv = (TextView)MyView.findViewById(R.id.grid_item_text);

            String nom=_ListFlacons.get(position).getName();
            tv.setText(nom);

            // Add The Image!!!
          ImageView imgView = (ImageView)MyView.findViewById(R.id.grid_item_image);
            URL url = null;
            Bitmap bmp = null;
            try {
                String path = "http://"+ VtNameUtils.DATABASE_HOST+"/"+VtNameUtils.SERVER_PHOTOS_DIRECTORY_NAME+"/";
                url = new URL(path+nom+".jpg");
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (MalformedURLException e) {
                e.printStackTrace();

            }catch (IOException e) {
                e.printStackTrace();
            }
            imgView.setImageBitmap(bmp);

         //   imgView.setImageResource(mThumbIds[position]);

        }

        return MyView;
    }

    // reference des images contenu dans res/drawable : ce sont les noms des fichiers
    private Integer[] mThumbIds = {
            R.drawable.sample_2, R.drawable.sample_3,
            R.drawable.sample_4, R.drawable.sample_5,
            R.drawable.sample_6,
            R.drawable.sample_0, R.drawable.sample_1,
            R.drawable.sample_2, R.drawable.sample_3,
            R.drawable.sample_4, R.drawable.sample_5,
            R.drawable.sample_6,
            R.drawable.sample_0, R.drawable.sample_1,
            R.drawable.sample_2, R.drawable.sample_3,
            R.drawable.sample_4, R.drawable.sample_5,
            R.drawable.sample_6
    };

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }
}


