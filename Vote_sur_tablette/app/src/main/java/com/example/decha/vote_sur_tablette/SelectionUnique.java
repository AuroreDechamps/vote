package com.example.decha.vote_sur_tablette;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;
import android.os.*;

import com.example.decha.vote_sur_tablette.Adapters.VoteImageItemAdapter;
import com.example.decha.vote_sur_tablette.DAO.DbHelper;
import com.example.decha.vote_sur_tablette.DAO.DecodeJson;
import com.example.decha.vote_sur_tablette.Metier.Flacons;
import android.util.*;

import java.util.ArrayList;
import java.util.List;

public class SelectionUnique extends AppCompatActivity {
    View title;
    // cette variable va memorise la position de la grid selectionne en premier choix
    int choixPrecendent;
    int compteur;
    Menu menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection_unique);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        title=findViewById(R.id.title);
        setSupportActionBar(toolbar);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // Creation du grid view
        final GridView gridview = (GridView) findViewById(R.id.gridview);
       // gridview.setAdapter(new ImageAdapter(this));
        gridview.setAdapter(new VoteImageItemAdapter(this,1));// hajar
        //apparition d'un toast lors du clique sur l'image
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
          public void onItemClick(AdapterView<?> parent, View v,
                                  int position, long id) {

              //si element choisi n'est pas deja selectionne, le compteur est focement egale a 0
                      if(compteur<1) {
                          gridview.getChildAt(position).setBackgroundColor(Color.GRAY);
                          //on incremente le compteur
                          compteur++;
                          Toast.makeText(SelectionUnique.this, "it's ok !" + compteur,
                                  Toast.LENGTH_SHORT).show();
                          Log.w("youness ", "jiji");
                          StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                          StrictMode.setThreadPolicy(policy);
                          Log.w("youness ", "koko");
                          List<Flacons> ListFlaconsYns = DecodeJson.getAllFlacons();
                          Log.w("youness ", "sisi");
                          int i=ListFlaconsYns.size();
                          Toast.makeText(SelectionUnique.this,"  ji ji ji ji : "+i,Toast.LENGTH_SHORT).show();
                          Log.w("hajar   ", " ******  "+i);
                          //on memorise la position du choix pour deselectionner
                          choixPrecendent = position;

                      }else{
                          //dans le cas ou l'on ne faut pas deselectionne le choix effectuer
                          if(choixPrecendent!=position) {
                              gridview.getChildAt(position).setBackgroundColor(Color.GRAY);
                              //on deselectionne le choix precendent
                              gridview.getChildAt(choixPrecendent).setBackgroundColor(Color.TRANSPARENT);
                              //on memorise le choix
                              choixPrecendent = position;
                          }else{
                              compteur--;
                              gridview.getChildAt(choixPrecendent).setBackgroundColor(Color.TRANSPARENT);

                          }


                      }
          }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_selection_unique, menu);
       this.menu=menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
       if (id==R.id.testBDD){
           Intent secondeActivite = new Intent(SelectionUnique.this,MesFlaconsActivity.class);
           startActivity(secondeActivite);
           return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
