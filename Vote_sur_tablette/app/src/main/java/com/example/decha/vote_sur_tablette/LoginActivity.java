package com.example.decha.vote_sur_tablette;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.decha.vote_sur_tablette.DAO.DecodeJson;
import com.example.decha.vote_sur_tablette.Metier.User;
import com.example.decha.vote_sur_tablette.Metier.Variable_Globales;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class LoginActivity extends AppCompatActivity {

    EditText txt_login;
    EditText txt_pass;
    Button btn_Login;
    Button btn_guest;
    TextView lb_message;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        txt_login=(EditText)findViewById(R.id.text_login);
        txt_pass=(EditText)findViewById(R.id.text_password);
        btn_Login=(Button)findViewById(R.id.btn_login);
        lb_message=(TextView)findViewById(R.id.lb_message_login);
        btn_guest=(Button)findViewById(R.id.btn_guest);

//
//        String serverFile = "http://10.0.2.2/VoteUpServer/ListPitures.php";
//        File myDir = new File(request.getRealPath(serverFile));
//        ArrayList fileArray = new ArrayList(Arrays.asList(myDir.list()));

        btn_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                User usr =   DecodeJson.getUserByLoginAndPass(txt_login.getText().toString(),txt_pass.getText().toString());
                if (usr!=null){
                    Variable_Globales.USER_CONNECTED =usr;
                    lb_message.setVisibility(View.INVISIBLE);
                    Intent nt = new Intent(LoginActivity.this, FirstActivity.class);
                    LoginActivity.this.startActivityForResult(nt, 19909);

                }else{
                    lb_message.setVisibility(View.VISIBLE);
                }

            }
        });


        btn_guest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Logged as a Guest!
                Intent nt = new Intent(LoginActivity.this, SwipeFragrance.class);
                LoginActivity.this.startActivityForResult(nt, 19909);
            }
        });
    }
}
