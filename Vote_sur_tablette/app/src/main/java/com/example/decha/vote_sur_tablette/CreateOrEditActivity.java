package com.example.decha.vote_sur_tablette;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class CreateOrEditActivity extends AppCompatActivity implements View.OnClickListener {

    // Les constantes
    final static int SELECT_PICTURE = 1;
    private VoteDBHelper dbHelper ;
    EditText nameEditText;
    EditText pictureEditText;
    ImageView picture;
    Button saveButton;
    LinearLayout buttonLayout;
    Button editButton, deleteButton;
    // Les éléments d'affichage
    ImageView imageVue;

    //Les variables
    public Bitmap bitmap;

    int flaconID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Puis on récupère l'iD donné dans l'autre activité
        flaconID = getIntent().getIntExtra(MesFlaconsActivity.KEY_EXTRA_CONTACT_ID, 0);

        setContentView(R.layout.activity_edit);
        nameEditText = (EditText) findViewById(R.id.editTextName);

        saveButton = (Button) findViewById(R.id.saveButton);
        saveButton.setOnClickListener(this);
        buttonLayout = (LinearLayout) findViewById(R.id.buttonLayout);
        editButton = (Button) findViewById(R.id.editButton);
        editButton.setOnClickListener(this);
        deleteButton = (Button) findViewById(R.id.deleteButton);
        deleteButton.setOnClickListener(this);

        dbHelper = new VoteDBHelper(this);

        if(flaconID > 0) {
            saveButton.setVisibility(View.GONE);
            buttonLayout.setVisibility(View.VISIBLE);

            Cursor rs = dbHelper.getFlacon(flaconID);
            rs.moveToFirst();
            String flaconName = rs.getString(rs.getColumnIndex(VoteDBHelper.FLACONS_COLUMN_NAME));
         //   String flaconPicture = rs.getString(rs.getColumnIndex(VoteDBHelper.FLACONS_COLUMN_PICTURE));
            




            if (!rs.isClosed()) {
                rs.close();
            }


            nameEditText.setText(flaconName);
            nameEditText.setFocusable(false);
            nameEditText.setClickable(false);

         //   pictureEditText.setText((CharSequence) flaconPicture);
//            pictureEditText.setFocusable(false);
//            pictureEditText.setClickable(false);

        }
        //super.onCreate(savedInstanceState);
    //    setContentView(R.layout.activity_edit);

        //Transforme la resource en bitmap
        bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.clik);

        //Initialise l'imageview on lui met une action
        imageVue = (ImageView) findViewById(R.id.imageVue);
        imageVue.setImageBitmap(bitmap);
        imageVue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btGalleryClick(v);
            }
        });



    }
    /*
    *Méthode pour ouvrir une galerie d'image
     */

    public void btGalleryClick(View v) {

        //creation et ouverture de la boite de dialogue
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, ""), SELECT_PICTURE);
    }

    /*
    *Retour du resultat de la galerie
     */
    protected void onActivityResult(int request, int resultCode, Intent data) {
        super.onActivityResult(request, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (request) {
                case SELECT_PICTURE:
                    String path = getRealPathFromUri(data.getData());
                    Log.d("Choix d'image", path);

                    //Transforme l'image en jpg
                    bitmap = BitmapFactory.decodeFile(path);

                    //affichage de la nouvelle image , mon problème se trouve là
                    //vu que ma nouvelle image ne s'affiche pas
                    imageVue.setImageBitmap(bitmap);

                    break;

            }
        }
    }

    /*
    *Methode pour récuperer l'Uri de l'image
     */
    private String getRealPathFromUri(Uri contentUri) {
        String result;

        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);

        if (cursor == null) {
            result = contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.saveButton:
                persistFlacon();
                return;
            case R.id.editButton:
                saveButton.setVisibility(View.VISIBLE);
                buttonLayout.setVisibility(View.GONE);
                nameEditText.setEnabled(true);
                nameEditText.setFocusableInTouchMode(true);
                nameEditText.setClickable(true);

//                pictureEditText.setEnabled(true);
  //              pictureEditText.setFocusableInTouchMode(true);
     //           pictureEditText.setClickable(true);

                return;
            case R.id.deleteButton:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.deletePicture)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dbHelper.deleteFlacon(flaconID);
                                Toast.makeText(getApplicationContext(), "Deleted Successfully", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(),MesFlaconsActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                            }
                        });
                AlertDialog d = builder.create();
                d.setTitle("Delete bottle ");
                d.show();
                return;
        }
    }

    public void persistFlacon() {
        if(flaconID > 0) {
            if(dbHelper.updateFlacon(flaconID, nameEditText.getText().toString(),
                    pictureEditText.getText().toString())) {
                Toast.makeText(getApplicationContext(), "bottle Update Successful", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(),MesFlaconsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            else {
                Toast.makeText(getApplicationContext(), "bottle Update Failed", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            if(dbHelper.insertFlacon(nameEditText.getText().toString(),"truc")) {
                Toast.makeText(getApplicationContext(), "bottle Inserted", Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(getApplicationContext(), "Could not Insert bottle", Toast.LENGTH_SHORT).show();
            }
            Intent intent = new Intent(getApplicationContext(), MesFlaconsActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }
}
