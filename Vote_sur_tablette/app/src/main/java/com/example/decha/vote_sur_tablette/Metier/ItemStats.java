package com.example.decha.vote_sur_tablette.Metier;

/**
 * Created by Admin on 11/05/2016.
 */

public class ItemStats {

    private int IdQuestionnaire;
    private int IdFlacon;
    private int NbVote;
    public ItemStats(){}
    public ItemStats(int IdQuestionnair,int idFlacon,int nbVote){

        IdQuestionnaire=IdQuestionnair;
        IdFlacon=idFlacon;
        NbVote=nbVote;
    }

    public int getIdIdQuestionnaire() {
        return IdQuestionnaire;
    }

    public int getIdFlacon() {
        return IdFlacon;
    }

    public int getNbVote() {
        return NbVote;
    }


    public void setIdQuestionnaire(int idQuestionnaire) {
        IdQuestionnaire = idQuestionnaire;
    }

    public void setIdFlacon(int idFlacon) {
        IdFlacon = idFlacon;
    }

    public void setNbVote(int nbVote) {
        NbVote = nbVote;
    }
}

