package com.example.decha.vote_sur_tablette;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.decha.vote_sur_tablette.DAO.DecodeJson;
import com.example.decha.vote_sur_tablette.DAO.VtNameUtils;
import com.example.decha.vote_sur_tablette.Metier.Fragrance;
import com.lorentzos.flingswipe.SwipeFlingAdapterView;


import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class SwipeFragrance extends AppCompatActivity {


        public static MyAppAdapter myAppAdapter;
        public static ViewHolder viewHolder;
        private ArrayList<Fragrance> al;
        private int i;

        @InjectView(R.id.frame) SwipeFlingAdapterView flingContainer;

        int imageNumber = 0;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_swiper);
            ButterKnife.inject(this);


            //Liste qui contient toutes les photos
            al = new ArrayList<>();
            al.add(new Fragrance("http://"+ VtNameUtils.DATABASE_HOST+"/"+VtNameUtils.SERVER_DIRECTORY_NAME+"/Pics/7.gif", ""));
            al.add(new Fragrance("http://"+ VtNameUtils.DATABASE_HOST+"/"+VtNameUtils.SERVER_DIRECTORY_NAME+"/Pics/2.jpg", ""));
            al.add(new Fragrance("http://"+ VtNameUtils.DATABASE_HOST+"/"+VtNameUtils.SERVER_DIRECTORY_NAME+"/Pics/3.jpg", ""));
            al.add(new Fragrance("http://"+ VtNameUtils.DATABASE_HOST+"/"+VtNameUtils.SERVER_DIRECTORY_NAME+"/Pics/4.jpg", ""));
            al.add(new Fragrance("http://"+ VtNameUtils.DATABASE_HOST+"/"+VtNameUtils.SERVER_DIRECTORY_NAME+"/Pics/5.jpg", ""));
            al.add(new Fragrance("http://"+ VtNameUtils.DATABASE_HOST+"/"+VtNameUtils.SERVER_DIRECTORY_NAME+"/Pics/6.jpg", ""));

            myAppAdapter = new MyAppAdapter(al, SwipeFragrance.this);
            flingContainer.setAdapter(myAppAdapter);


            flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener() {
                @Override
                public void removeFirstObjectInAdapter() {
                    // this is the simplest way to delete an object from the Adapter (/AdapterView)
                    Log.d("LIST", "removed object!");
                    al.remove(0);
                    myAppAdapter.notifyDataSetChanged();
                }

                @Override
                public void onLeftCardExit(Object dataObject) {
                    //Do something on the left!
                    //You also have access to the original object.
                    //If you want to use it just cast it (String) dataObject
                    imageNumber++;
                    //makeToast(SwipeFragrance.this, "Left!");
                }

                @Override
                public void onRightCardExit(Object dataObject) {
                    imageNumber++;
                    DecodeJson.VoterPourFlacon(imageNumber);
                    //makeToast(SwipeFragrance.this, "Right!");
                }

                @Override
                public void onAdapterAboutToEmpty(int itemsInAdapter) {
                        if(imageNumber == VtNameUtils.NUMBER_OF_IMAGES){
                            AlertDialog alertDialog = new AlertDialog.Builder(SwipeFragrance.this).create();
                            alertDialog.setTitle(VtNameUtils.END_VOTE_TITLE);
                            alertDialog.setMessage(VtNameUtils.END_VOTE_MESSAGE);
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Retour",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent nt = new Intent(SwipeFragrance.this, LoginActivity.class);
                                            SwipeFragrance.this.startActivityForResult(nt, 19909);
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialog.show();
                        }
               }

                @Override
                public void onScroll(float scrollProgressPercent) {
                    View view = flingContainer.getSelectedView();
                }
            });


            // Optionally add an OnItemClickListener
            flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
                @Override
                public void onItemClicked(int itemPosition, Object dataObject) {
                    //makeToast(SwipeFragrance.this, "Clicked!");

                }
            });

        }

        static void makeToast(Context ctx, String s){
            Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show();
        }



        @OnClick(R.id.right)
        public void right() {
            /**
             * Trigger the right event manually.
             */
            flingContainer.getTopCardListener().selectRight();
        }

        @OnClick(R.id.left)
        public void left() {
            flingContainer.getTopCardListener().selectLeft();
        }

    public static class ViewHolder {
        public static FrameLayout background;
        public TextView DataText;
        public ImageView cardImage;


    }

    public class MyAppAdapter extends BaseAdapter {


        public List<Fragrance> parkingList;
        public Context context;

        private MyAppAdapter(List<Fragrance> apps, Context context) {
            this.parkingList = apps;
            this.context = context;
        }

        @Override
        public int getCount() {
            return parkingList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View rowView = convertView;


            if (rowView == null) {

                LayoutInflater inflater = getLayoutInflater();
                rowView = inflater.inflate(R.layout.item, parent, false);
                // configure view holder
                viewHolder = new ViewHolder();
                viewHolder.DataText = (TextView) rowView.findViewById(R.id.bookText);
                viewHolder.background = (FrameLayout) rowView.findViewById(R.id.background);
                viewHolder.cardImage = (ImageView) rowView.findViewById(R.id.cardImage);
                rowView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.DataText.setText(parkingList.get(position).getDescription() + "");

            Glide.with(SwipeFragrance.this).load(parkingList.get(position).getImagePath()).into(viewHolder.cardImage);

            return rowView;
        }
    }




}
