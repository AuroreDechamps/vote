package com.example.decha.vote_sur_tablette;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class CreateOrEditQuestionnaireActivity extends AppCompatActivity {

    Button  btnValider;
    TextView  lbTitreForm;
    TextView  lb_ID;
    EditText txt_Titre;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_or_edit_questionnaire);

        txt_Titre=(EditText)findViewById(R.id.text_titre_vote_edit);
        lbTitreForm=(TextView)findViewById(R.id.lb_titre_form_edit_vote);
        lb_ID=(TextView)findViewById(R.id.lb_id_form_edit_vote);
        btnValider=(Button)findViewById(R.id.btn_valider_vote);

        btnValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }
}
