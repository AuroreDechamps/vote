
package com.example.decha.vote_sur_tablette.DAO;

import com.example.decha.vote_sur_tablette.Metier.ItemStats;
import com.example.decha.vote_sur_tablette.Metier.User;
import com.example.decha.vote_sur_tablette.Metier.Flacons;
import com.example.decha.vote_sur_tablette.Metier.Questionnaire;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;

import java.util.Date;

public class DecodeJson {

	static final String url = "http://"+VtNameUtils.DATABASE_HOST+"/"+VtNameUtils.SERVER_DIRECTORY_NAME+"/FlaconsController.php";

	public static List<User> getAllUsers() {

		List<User> l = new ArrayList<User>();
		try {
			Bundle params = new Bundle();
			params.putString("action", "getAllUsers");
			String response;
			response = Util.openUrl(url, "GET", params);
			JSONArray jArray = new JSONArray(response);
			for (int j = 0; j < jArray.length(); j++) {
				JSONObject oneObject = jArray.getJSONObject(j);
				// Pulling items from the array
				User u = new User();
				u.setId( Integer.parseInt(oneObject.getString("id")));
                                u.setNom(oneObject.getString("name"));
                                u.setEmail(oneObject.getString("email"));

				l.add(u);

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return l;
	}

        public static List<Flacons> getCountGroupeFlaconsNbVote() {
		List<Flacons> l = new ArrayList<Flacons>();
		try {
			Bundle params = new Bundle();
			params.putString("action", "getAllUsers");
			String response;
			response = Util.openUrl(url, "GET", params);
			JSONArray jArray = new JSONArray(response);
			for (int j = 0; j < jArray.length(); j++) {
				JSONObject oneObject = jArray.getJSONObject(j);
				// Pulling items from the array
				Flacons f = new Flacons();
				f.setID(Integer.parseInt(oneObject.getString("id")));
				f.setName(oneObject.getString("name"));
				f.setPicture(oneObject.getString("picture"));
				l.add(f);
                        }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return l;

        }

		public static List<Flacons> getCountGroupeFlaconsNbVoteParVote(int IdVote) {
			List<Flacons> l = new ArrayList<Flacons>();
			try {
				Bundle params = new Bundle();
				params.putString("action", "getAllUsers");
				String response;
				response = Util.openUrl(url, "GET", params);
				JSONArray jArray = new JSONArray(response);
				for (int j = 0; j < jArray.length(); j++) {
					JSONObject oneObject = jArray.getJSONObject(j);
					// Pulling items from the array
					Flacons f = new Flacons();
					f.setID(Integer.parseInt(oneObject.getString("id")));
					f.setName(oneObject.getString("name"));
					f.setPicture(oneObject.getString("picture"));
					l.add(f);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return l;

		}

		public static ArrayList<Flacons> getAllFlaconsByIdQuestionnaire(int idVote) {
				String url = "http://"+VtNameUtils.DATABASE_HOST+"/"+VtNameUtils.SERVER_DIRECTORY_NAME+"/"+VtNameUtils.SERVER_FLACONS_CONTROLLER;
						ArrayList<Flacons> l = new ArrayList<Flacons>();
				try {
					Bundle params = new Bundle();
					params.putString("action", "getAllFlaconsByIdQuestionnaire");
					params.putString("IdQuestionnaire", idVote+"");
					String response;
					response = Util.openUrl(url, "GET", params);
					JSONArray jArray = new JSONArray(response);
					for (int j = 0; j < jArray.length(); j++) {
						JSONObject oneObject = jArray.getJSONObject(j);
						// Pulling items from the array
						Flacons f = new Flacons();
						f.setID( Integer.parseInt(oneObject.getString("id")));
						f.setName(oneObject.getString("name"));
						f.setPicture(oneObject.getString("picture"));
						l.add(f);

					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return l;
		}


        public static List<Flacons> getAllFlacons() {
		String url = "http://"+VtNameUtils.DATABASE_HOST+"/"+VtNameUtils.SERVER_DIRECTORY_NAME+"/"+VtNameUtils.SERVER_FLACONS_CONTROLLER;
		List<Flacons> l = new ArrayList<Flacons>();
		try {
			Bundle params = new Bundle();
			params.putString("action", "getAllFlacons");
			String response;
			response = Util.openUrl(url, "GET", params);
			JSONArray jArray = new JSONArray(response);
			for (int j = 0; j < jArray.length(); j++) {

				JSONObject oneObject = jArray.getJSONObject(j);
				// Pulling items from the array
				Flacons f = new Flacons();
				f.setID( Integer.parseInt(oneObject.getString("id")));
				f.setName(oneObject.getString("name"));
				f.setPicture(oneObject.getString("picture"));
				l.add(f);

                        }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return l;
        }

	public static ArrayList<Flacons> getAllFlaconsAR() {
		String url = "http://"+VtNameUtils.DATABASE_HOST+"/"+VtNameUtils.SERVER_DIRECTORY_NAME+"/"+VtNameUtils.SERVER_FLACONS_CONTROLLER;
		ArrayList<Flacons> l = new ArrayList<Flacons>();
		try {
			Bundle params = new Bundle();
			params.putString("action", "getAllFlacons");
			String response;
			response = Util.openUrl(url, "GET", params);
			JSONArray jArray = new JSONArray(response);
			for (int j = 0; j < jArray.length(); j++) {

				JSONObject oneObject = jArray.getJSONObject(j);
				// Pulling items from the array
				Flacons f = new Flacons();
				f.setID( Integer.parseInt(oneObject.getString("id")));
				f.setName(oneObject.getString("name"));
				f.setPicture(oneObject.getString("picture"));
				l.add(f);

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return l;
	}

	public static ArrayList<Questionnaire> getAllVotes() {//  hadi quesionnaires , titre de foction à changer !:
		String url = "http://"+VtNameUtils.DATABASE_HOST+"/"+VtNameUtils.SERVER_DIRECTORY_NAME+"/"+VtNameUtils.SERVER_QUSETIONNAIRES_CONTROLLER;
		ArrayList<Questionnaire> l = new ArrayList<Questionnaire>();
		try {
			Bundle params = new Bundle();
			params.putString("action", "getAllQuestionnaire");
			String response;
			response = Util.openUrl(url, "GET", params);
			JSONArray jArray = new JSONArray(response);
			for (int j = 0; j < jArray.length(); j++) {
				JSONObject oneObject = jArray.getJSONObject(j);
				// Pulling items from the array
				Questionnaire f = new Questionnaire();
				f.setIDQuestionnaire(Integer.parseInt(oneObject.getString("id")));
				f.setTitreQuestionnaire(oneObject.getString("name"));
				f.setFotoQuestionnaire(oneObject.getString("picture"));
				l.add(f);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return l;
	}


	public static ArrayList<ItemStats> getAllStatistiquesByIdQuestionnaire(int idQuestionnaire) {
		String url = "http://"+VtNameUtils.DATABASE_HOST+"/"+VtNameUtils.SERVER_DIRECTORY_NAME+"/"+VtNameUtils.SERVER_VOTES_CONTROLLER;
		ArrayList<ItemStats> l = new ArrayList<ItemStats>();
		try {
			Bundle params = new Bundle();
			params.putString("action", "getStatsFlaconsByIdQuestionnaire");
			params.putString("IdQuestionnaire", ""+idQuestionnaire);
			String response;
			response = Util.openUrl(url, "GET", params);
			JSONArray jArray = new JSONArray(response);
			for (int j = 0; j < jArray.length(); j++) {
				JSONObject oneObject = jArray.getJSONObject(j);
				// Pulling items from the array
				ItemStats f = new ItemStats();
				f.setIdQuestionnaire(idQuestionnaire);
				f.setIdFlacon(Integer.parseInt(oneObject.getString("idFalconVote")));
				f.setNbVote(Integer.parseInt(oneObject.getString("nombre")));
				l.add(f);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return l;
	}


	public static int AddVote(String TitreVote){
		int idAdded=0;
		String url = "http://"+VtNameUtils.DATABASE_HOST+"/"+VtNameUtils.SERVER_DIRECTORY_NAME+"/"+VtNameUtils.SERVER_VOTES_CONTROLLER;
		List<Flacons> l = new ArrayList<Flacons>();
		try {
			Bundle params = new Bundle();
			params.putString("action", "AddVote");
			params.putString("titreVote", TitreVote);
			String response;
			response = Util.openUrl(url, "GET", params);



		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return idAdded;
	}

	public static void VoterPourFlacon(final int idFlacon){


		Thread thread = new Thread(new Runnable(){

			String url = "http://"+VtNameUtils.DATABASE_HOST+"/"+VtNameUtils.SERVER_DIRECTORY_NAME+"/"+VtNameUtils.SERVER_FLACONS_CONTROLLER;

			public void run() {
				try {
					Bundle params = new Bundle();
					params.putString("action", "VoterPourFlacon");
					params.putString("flacon", idFlacon+"");
					String response;

					response = Util.openUrl(url, "GET", params);
					// reste le traitement de message de retour / une ligne :)
					//JSONArray jArray = new JSONArray(response);

				} catch (Exception e) {
					// TODO Auto-generated catch block
					Log.e("error loading", "E");
					e.printStackTrace();
				}
			}
		});

		thread.start();

	}

	public  static void SuprimerFlacon(int idFlacon){
		try {
			Bundle params = new Bundle();
			params.putString("action", "SupprimerFlacon");
			params.putString("idFlacon", idFlacon+"");
			String response;
			response = Util.openUrl(url, "GET", params);

			// reste le traitement de message de retour / une ligne :)
			//JSONArray jArray = new JSONArray(response);


		} catch (Exception e) {
			// TODOAuto-generated catch block

			e.printStackTrace();
		}
	}

	public  static void SuprimerQuestionnaire(int idFlacon){
		try {
			Bundle params = new Bundle();
			params.putString("action", "SupprimerQuestionnaire");
			params.putString("idQuestionnaire", idFlacon+"");
			String response;
			response = Util.openUrl(url, "GET", params);
			// reste le traitement de message de retour / une ligne :)
			//JSONArray jArray = new JSONArray(response);


		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static User getUserByLoginAndPass(String login, String pass) {
		String url = "http://"+VtNameUtils.DATABASE_HOST+"/"+VtNameUtils.SERVER_DIRECTORY_NAME+"/UserController.php";
		List<User> l = new ArrayList<User>();
		Bundle params = new Bundle();
		params.putString("action", "getUserByLoginAndPass");
		params.putString("login", login);
		params.putString("pass", pass);
		String response;
		User u = new User();
		JSONObject oneObject = null;
		try {
			response = Util.openUrl(url, "GET", params);
			JSONArray jArray = new JSONArray(response);
			oneObject = jArray.getJSONObject(0);
			u.setId( Integer.parseInt(oneObject.getString("id")));
			u.setNom(oneObject.getString("name"));
			u.setEmail(oneObject.getString("email"));
		} catch (JSONException j){
			// TODO Auto-generated catch block
			j.printStackTrace();
			return null;
		}

		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return u;
	}

}
