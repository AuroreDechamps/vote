package com.example.decha.vote_sur_tablette;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.decha.vote_sur_tablette.Adapters.StatsVoteAdapter;

public class SimpleStatsActivity extends AppCompatActivity {
    ListView  _listview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_stats);

        _listview=(ListView)findViewById(R.id.list_view_votes_stats);

        _listview.setAdapter(new StatsVoteAdapter(this));

        _listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){

                    case  0:
                        Intent nt = new Intent(SimpleStatsActivity.this, CreateOrEditQuestionnaireActivity.class);
                        SimpleStatsActivity.this.startActivityForResult(nt, 19909);
                        break;

                }
            }
        });
    }
}
