package com.example.decha.vote_sur_tablette.Adapters;

import android.content.Context;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.decha.vote_sur_tablette.DAO.DbHelper;
import com.example.decha.vote_sur_tablette.Metier.ItemMenuPrincipale;
import com.example.decha.vote_sur_tablette.R;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Admin on 10/05/2016.
 */
public class MenuAdapter extends  BaseAdapter
{
    Context MyContext;

    ArrayList<ItemMenuPrincipale> MenuPrincipale=new ArrayList<ItemMenuPrincipale>();

    public MenuAdapter(Context _MyContext)
    {
        MyContext = _MyContext;
        MenuPrincipale.add(new ItemMenuPrincipale(1,"Nouveau Questionnaire", R.drawable.add));
        MenuPrincipale.add(new ItemMenuPrincipale(1,"Liste Questionnaire",R.drawable.lista));
        MenuPrincipale.add(new ItemMenuPrincipale(1,"Statistiques",R.drawable.chart));
        MenuPrincipale.add(new ItemMenuPrincipale(1,"Quitter",R.drawable.quitter));
    }

    @Override
    public int getCount()
    {
        return MenuPrincipale.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View MyView = convertView;

        if ( convertView == null )
        {

            ItemMenuPrincipale  itemMenu=MenuPrincipale.get(position);

            LayoutInflater li = LayoutInflater.from(MyContext);
            MyView = li.inflate(R.layout.menu_item, null);
            // Add The Text!!!
            TextView tv = (TextView)MyView.findViewById(R.id.text_menu_item);
            String titre=itemMenu.getTitre();
            tv.setText(titre);
            ImageView imgView = (ImageView)MyView.findViewById(R.id.image_menu_item);
            imgView.setImageResource(itemMenu.getIdPhotofromDrawable());
        }
        return MyView;
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }
}
