package com.example.decha.vote_sur_tablette;

import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.app.AlertDialog;

import com.example.decha.vote_sur_tablette.Adapters.AdminListFlaconsItemAdapter;
import com.example.decha.vote_sur_tablette.Adapters.MenuAdapter;
import com.example.decha.vote_sur_tablette.Metier.Variable_Globales;

public class AdminListeFlaconsActivity extends AppCompatActivity {
    ListView ListViewFlaconAdmin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_liste_flacons);

        ListViewFlaconAdmin=(ListView)findViewById(R.id.list_view_flacon_admin_votes);
        ListViewFlaconAdmin.setAdapter(new AdminListFlaconsItemAdapter(this, Variable_Globales.ID_QUESTIONNAIRE_SELECTED));
        ListViewFlaconAdmin.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                AlertDialog.Builder ad = new AlertDialog.Builder(
                        AdminListeFlaconsActivity.this);
                ad.setIcon(R.drawable.add);
                ad.setTitle("Voulez-vous vraiment le supprimer ??!!");
                ad.setPositiveButton("oui", new android.content.DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {

                    }
                });
                ad.setNegativeButton("Non", new android.content.DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog,
                                        int which) {

                    }
                });
                ad.show();

            }
        });

    }
}
