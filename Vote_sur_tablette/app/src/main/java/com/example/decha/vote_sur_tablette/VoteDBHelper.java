package com.example.decha.vote_sur_tablette;

import android.database.sqlite.SQLiteOpenHelper;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by decha on 18/02/2016.
 */
public class VoteDBHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "SQLite.db";
    private static final int DATABASE_VERSION = 2;

    public static final String FLACONS_TABLE_NAME = "flacons";
    public static final String FLACONS_COLUMN_ID = "_id";
    public static final String FLACONS_COLUMN_NAME = "name";
    public static final String FLACONS_COLUMN_PICTURE = "picture";
    public static final String FLACONS_COLUMN_NUMBER_OF_VOTE = "number of vote";

    public VoteDBHelper(Context context) {
        super(context, DATABASE_NAME , null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE " + FLACONS_TABLE_NAME +
                        "(" +FLACONS_COLUMN_ID + " INTEGER PRIMARY KEY, " +
                        FLACONS_COLUMN_NAME + " TEXT NOT NULL, "+
                        FLACONS_COLUMN_NUMBER_OF_VOTE + " INTEGER)"
        );
    }

    //methode appele lors de la premiere utilisation de la appli
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + FLACONS_TABLE_NAME);
        onCreate(db);
    }

    public boolean insertFlacon(String name, String picture) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(FLACONS_COLUMN_NAME, name);
       // contentValues.put(FLACONS_COLUMN_PICTURE, picture);


        db.insert(FLACONS_TABLE_NAME, null, contentValues);
        return true;
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, FLACONS_TABLE_NAME);
        return numRows;
    }

    public boolean updateFlacon(Integer id, String name, String picture) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(FLACONS_COLUMN_NAME, name);
      //  contentValues.put(FLACONS_COLUMN_PICTURE, picture);
        db.update(FLACONS_TABLE_NAME, contentValues, FLACONS_COLUMN_ID + " = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public Integer deleteFlacon(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(FLACONS_TABLE_NAME,
                FLACONS_COLUMN_ID + " = ? ",
                new String[] { Integer.toString(id) });
    }

    public Cursor getFlacon(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery("SELECT * FROM " + FLACONS_TABLE_NAME + " WHERE " +
                FLACONS_COLUMN_ID + "=?", new String[]{Integer.toString(id)});
        return res;
    }

    public Cursor getAllFlacons() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "SELECT * FROM " + FLACONS_TABLE_NAME, null );
        return res;
    }

}
