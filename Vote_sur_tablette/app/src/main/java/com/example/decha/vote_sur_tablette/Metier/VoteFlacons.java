package com.example.decha.vote_sur_tablette.Metier;

import java.util.Date;


public class VoteFlacons {
    
    private int id;
    private int idFlacons;
    private Date DateVote;

    public VoteFlacons() {
    }

    
    public VoteFlacons(int id, int idFlacons, Date DateVote) {
        this.id = id;
        this.idFlacons = idFlacons;
        this.DateVote = DateVote;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdFlacons() {
        return idFlacons;
    }

    public void setIdFlacons(int idFlacons) {
        this.idFlacons = idFlacons;
    }

    public Date getDateVote() {
        return DateVote;
    }

    public void setDateVote(Date DateVote) {
        this.DateVote = DateVote;
    }
    
    
    
}
