package com.example.decha.vote_sur_tablette;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.decha.vote_sur_tablette.Adapters.MenuAdapter;

public class FirstActivity extends AppCompatActivity {

    ListView ListViewMenu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        ListViewMenu=(ListView)findViewById(R.id.list_view_menu);
        ListViewMenu.setAdapter(new MenuAdapter(this));


        ListViewMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){

                    case  0:
                        Intent nt = new Intent(FirstActivity.this, CreateOrEditQuestionnaireActivity.class);
                        FirstActivity.this.startActivityForResult(nt, 19909);
                       break;
                    case  1:
                        Intent nt2 = new Intent(FirstActivity.this, AdminListeQuestionnaireActivity.class);
                        FirstActivity.this.startActivityForResult(nt2, 19919);
                        break;
                    case  2:
                        Intent ntstat= new Intent(FirstActivity.this, SimpleFlaconStatsActivity.class);
                        FirstActivity.this.startActivityForResult(ntstat, 19921);
                        break;
                    case  3:
                        finishAffinity();
                        break;
                }
            }
        });
    }
}
