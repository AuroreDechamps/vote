package com.example.decha.vote_sur_tablette.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.decha.vote_sur_tablette.DAO.DecodeJson;
import com.example.decha.vote_sur_tablette.DAO.VtNameUtils;
import com.example.decha.vote_sur_tablette.Metier.ItemStats;
import com.example.decha.vote_sur_tablette.Metier.Questionnaire;
import com.example.decha.vote_sur_tablette.R;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Admin on 11/05/2016.
 */
public class StatsVoteAdapter  extends BaseAdapter
{
    Context MyContext;

    ArrayList<Questionnaire> ListeVotes=new ArrayList<Questionnaire>();
    ArrayList<ItemStats> ListsStats=new ArrayList<ItemStats>();
    public StatsVoteAdapter(Context _MyContext)
    {
        MyContext = _MyContext;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        ListeVotes=DecodeJson.getAllVotes();
    }

    @Override
    public int getCount()
    {
        return ListeVotes.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View MyView = convertView;
        if ( convertView == null )
        {
            Questionnaire itemVote=ListeVotes.get(position);
            LayoutInflater li = LayoutInflater.from(MyContext);
            MyView = li.inflate(R.layout.item_stats_questionnaire, null);
            // Add The Text!!!
            TextView tv = (TextView)MyView.findViewById(R.id.text_vote_item_stats);
            String titre=itemVote.getTitreQuestionnaire();
            tv.setText(titre);
            ImageView imgView = (ImageView)MyView.findViewById(R.id.image_vote_item_stats);
            URL url = null;
            Bitmap bmp = null;
            try {
                String path = "http://"+ VtNameUtils.DATABASE_HOST+"/"+VtNameUtils.SERVER_PHOTOS_DIRECTORY_NAME+"/";
                url = new URL(path+itemVote.getFotoQuestionnaire());
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }catch (IOException e) {
                e.printStackTrace();
            }
            imgView.setImageBitmap(bmp);
            // imgView.setImageResource(itemVote.getIdPhotofromDrawable());
        }
        return MyView;
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }
}