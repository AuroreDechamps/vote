package com.example.decha.vote_sur_tablette.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.decha.vote_sur_tablette.DAO.DecodeJson;
import com.example.decha.vote_sur_tablette.DAO.VtNameUtils;
import com.example.decha.vote_sur_tablette.Metier.Flacons;
import com.example.decha.vote_sur_tablette.R;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Admin on 11/05/2016.
 */
public class AdminListFlaconsItemAdapter extends BaseAdapter
{
    Context MyContext;

    ArrayList<Flacons> Listeflacons=new ArrayList<Flacons>();

    public AdminListFlaconsItemAdapter(Context _MyContext,int Idquestionnaire)
    {
        MyContext = _MyContext;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Listeflacons= DecodeJson.getAllFlaconsByIdQuestionnaire(Idquestionnaire);
    }

    @Override
    public int getCount()
    {
        return Listeflacons.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View MyView = convertView;

        if ( convertView == null )
        {

            Flacons  itemVote=Listeflacons.get(position);
            LayoutInflater li = LayoutInflater.from(MyContext);
            MyView = li.inflate(R.layout.menu_item, null);
            // Add The Text!!!
            TextView tv = (TextView)MyView.findViewById(R.id.text_flacon_item_admin);
            String titre=itemVote.getName();
            tv.setText(titre);
            ImageView imgView = (ImageView)MyView.findViewById(R.id.image_flacon_item_admin);
            URL url = null;
            Bitmap bmp = null;
            try {
                String path = "http://"+ VtNameUtils.DATABASE_HOST+"/"+VtNameUtils.SERVER_DIRECTORY_NAME +"/"+VtNameUtils.SERVER_PHOTOS_DIRECTORY_NAME+"/";
                url = new URL(path+itemVote.getPicture());
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }catch (IOException e) {
                e.printStackTrace();
            }
            imgView.setImageBitmap(bmp);
            // imgView.setImageResource(itemVote.getIdPhotofromDrawable());
        }
        return MyView;
    }

    @Override
    public Object getItem(int position) {

        return Listeflacons.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return Listeflacons.get(position).getID();
    }
}