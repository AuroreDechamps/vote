
package com.example.decha.vote_sur_tablette.DAO;


public class VtNameUtils {
    
    public static final String DATABASE_HOST = "mah-dif.com";//10.0.2.2id a changer a chaque redemarrage du pc pr le moment en attendant d acheter une ad ip fix
    public static final String SERVER_DIRECTORY_NAME = "VoteUpServer";
    public static final String SERVER_PHOTOS_DIRECTORY_NAME="Pics";
    public static final String SERVER_FLACONS_CONTROLLER = "FlaconsController.php";// Oh my GOD
    public static final String SERVER_VOTES_CONTROLLER = "VotesControlleur.php";// Oh my GOD
    public static final String SERVER_QUSETIONNAIRES_CONTROLLER = "QuestionnaireController.php";// Oh my GOD
    public static final String DATABASE_NAME = "votedatabase";
    public static final String DATABASE_LOGIN = "root";
    public static final String DATABASE_PASSWORD = "";
    public static final int DATABASE_VERSION = 2;

    public static final String FLACONS_TABLE_NAME = "flacons";
    public static final String FLACONS_COLUMN_ID = "_id";
    public static final String FLACONS_COLUMN_NAME = "name";
    public static final String FLACONS_COLUMN_PICTURE = "picture";
    //public static final String FLACONS_COLUMN_NUMBER_OF_VOTE = "number of vote";
    
    public static final String VOTE_TABLE_NAME = "flacons";
    public static final String VOTE_COLUMN_ID = "_id";

    public static final String END_VOTE_TITLE = "Vote terminé";
    public static final String END_VOTE_MESSAGE = "Merci pour votre participation !";
    public static final int NUMBER_OF_IMAGES = 6;
    
}
