<?php


$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "vote_sur_tablette";
// Create connection


$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 


// Check connection

$sql = "SELECT _id, name, picture FROM flacons";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
	
	$data = array();
	$i=0;
    while($row = $result->fetch_assoc()) {
        $id=$row["_id"]; 
		$name=$row["name"]; 
		$picture=$row["picture"];
		
		$data[$i++] =(object)array(
        'id' => $id,
        'name' => $name,
		'picture' => $picture,
									);
    }
	$rep = json_encode($data);
				echo $rep;
} else {
    echo "0 results";
}


$conn->close();
?>