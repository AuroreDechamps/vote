<?php


$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "vote_sur_tablette";
// Create connection

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

if (isset($_REQUEST['action'])) { 
	$action = $_REQUEST['action'];
	$data = array();
        if ($action == "getAllFlacons") {
    
	$sql = "SELECT _id, name, picture FROM flacons";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
	    // output data of each row
		
		$data = array();
		$i=0;
	    while($row = $result->fetch_assoc()) {
		$id=$row["_id"]; 
			$name=$row["name"]; 
			$picture=$row["picture"];
			
			$data[$i++] =(object)array(
		'id' => $id,
		'name' => $name,
			'picture' => $picture,
					   );
	    }
	$rep = json_encode($data);
	echo $rep;
} else {
    echo "0 results";
} 
}

if ($action == "getAllFlaconsByIdQuestionnaire") {
    
	$idQuestionnaire=$_REQUEST["IdQuestionnaire"]; 
	$sql1 = "SELECT _id, name, picture,id_questionnaire FROM flacons where id_questionnaire=".$idQuestionnaire; 
	$result2 = $conn->query($sql1); 
	if ($result2->num_rows > 0) {
	    // output data of each row
		$data = array();
		$i=0;
	    while($row = $result2->fetch_assoc()) {
		$id=$row["_id"]; 
			$name=$row["name"]; 
			$picture=$row["picture"];
			$id_questionnaire=$row["id_questionnaire"];
			$data[$i++] =(object)array(
		'id' => $id,
		'name' => $name,
			'picture' => $picture,
			'id_questionnaire'=>$id_questionnaire,
					   );
	    }
	$rep = json_encode($data);
	echo $rep;
} else {
    echo "0 results";
} 
}

if ($action == "AddFlacon") {
// Check connection 
//$idFlacons = $_REQUEST['IdFlacon'];
$name = $_REQUEST['nameFlacon'];
$photo = $_REQUEST['photoflacon'];
$idQuestionnaire = $_REQUEST['idQuestionnaire'];
$sql = "INSERT INTO voteflacons ( idFalconVote, dateVote) VALUES ( '".$idFlacons."', now())";
$result = $conn->query($sql); 
$rep = json_encode($conn->insert_id);
echo $rep;
}


if ($action == "VoterPourFlacon") {
// Check connection 
$idFlacons = $_GET["flacon"];
$sql = "INSERT INTO voteflacons ( idFalconVote, dateVote) VALUES ( '".$idFlacons."', now())";
$result = $conn->query($sql); 
$rep = json_encode($conn->insert_id);
echo $rep;
}


if ($action == "SupprimerFlacon") {
// Check connection 
$idFlacons = $_REQUEST['IdFlacon'];
$sql = "delete from Flacons where _id=".$idFlacons;
$result = $conn->query($sql);  
echo $rep;
}


}
$conn->close();
?>